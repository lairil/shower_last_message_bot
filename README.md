# Project Description

This project is a Telegram bot that tracks the last message time of each user
in a chat and reports who hasn't sent a message in a while.

# Usage

To use this bot, you will need to create your own bot and get a token. You can get a token by following the instructions on the official Telegram page for creating bots.

In addition, you must install the following dependencies:

-   sqlite3
-   datetime
-   python-telegram-bot

To start the bot, you need to run the `shlang_bot.py` script, which defines the `main()` function. 
This function creates a connection to the database, creates a users table, creates an Updater object, and adds command and message handlers. 
Then the bot starts working by polling incoming updates.

The bot responds to the `/last_messages` command sent by any chat participant.
It sends a message listing the users who haven't sent messages in the last 12 hours.
If a user hasn't sent messages for more than 12 hours, the bot sends them a private message notifying them that they are a "hose".

## Installation and Usage

1.  Download the repository or clone it:
    `git  clone  https://github.com/<username>/shlang-bot.git`
    
2.  Install the dependencies specified in the `requirements.txt` file.
    `pip install -r requirements.txt`
    
3.  Create a `config.py` file in the root directory and add your bot token:
    `TOKEN =  'your_token'`
    
5.  Run the bot:
    `python shlang_bot.py`

## Commands

The hose-bot has the following command:

-   `/last_messages` - lists the users who haven't sent messages in the chat for a long time. If a user hasn't sent messages for more than 12 hours, the bot sends them a notification that they have become an official hose.