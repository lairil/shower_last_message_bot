FROM python:3.9.16-slim-buster

WORKDIR /bot

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .

RUN chmod +x shlang_bot.py

CMD [ "/bot/shlang_bot.py"]
ENTRYPOINT ["python3"]