import logging
import sqlite3
import datetime

from telegram.error import BadRequest
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from config import TOKEN


#  Function for add user to database
def add_user(chat_id, user_id, first_name, username):
    with sqlite3.connect('last_messages.db') as conn:
        c = conn.cursor()
        c.execute(
            "INSERT OR IGNORE INTO users (chat_id, user_id, first_name, username, last_message_date) VALUES (?, ?, ?, ?, ?)",
            (chat_id, user_id, first_name, username, datetime.datetime.now().isoformat()))
        conn.commit()


def check_user_in_chat(bot, chat_id, user_id):
    try:
        chat_member = bot.get_chat_member(chat_id, user_id)
        if bool(chat_member):
            return True
        else:
            with sqlite3.connect('last_messages.db') as conn:
                c = conn.cursor()
                c.execute("DELETE FROM users WHERE chat_id = ? AND user_id = ?", (chat_id, user_id))
                conn.commit()
            return False
    except BadRequest as e:
        # удаление записи из базы данных
        with sqlite3.connect('last_messages.db') as conn:
            c = conn.cursor()
            c.execute("DELETE FROM users WHERE chat_id = ? AND user_id = ?", (chat_id, user_id))
            conn.commit()
        print(f"An error occurred while checking user in chat: {e}")
        return False


# Function for update of date user's last message
def update_last_message_date(chat_id, user_id):
    with sqlite3.connect('last_messages.db') as conn:
        c = conn.cursor()
        c.execute("UPDATE users SET last_message_date = ? WHERE chat_id = ? AND user_id = ?",
                  (datetime.datetime.now().isoformat(), chat_id, user_id))
        conn.commit()


# Function for processing of command /last_messages
def handle_last_messages_command(update, context):
    with sqlite3.connect('last_messages.db') as conn:
        c = conn.cursor()
        c.execute("SELECT user_id, first_name, username, last_message_date FROM users WHERE chat_id = ?",
                  (update.message.chat_id,))
        users = c.fetchall()
    not_shangs_messages = ''

    for user in users:
        if check_user_in_chat(context.bot, update.message.chat_id, user[0]):
            last_message_date = datetime.datetime.fromisoformat(user[3])
            time_delta = datetime.datetime.now() - last_message_date
            hours, remainder = divmod(time_delta.seconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            if time_delta.total_seconds() < 12 * 3600:
                user_name = user[2] if user[2] else user[1]
                message_template = f'Последнее сообщение от пользователя {user_name} было написано {time_delta.days} дней, {hours} ' \
                               f'часов и {minutes} минут назад \n'
                not_shangs_messages += message_template
            else:
                user_name = "@" + user[2] if user[2] else f'<a href="tg://user?id={user[0]}">{user[1]}</a>' 
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text=f"Ты, {user_name}, официально шланг! Твое последнее сообщение было "
                                              f"{time_delta.days} дней, {hours} часов и {minutes} минут назад",
                                         parse_mode='HTML')
    context.bot.send_message(chat_id=update.effective_chat.id, text=not_shangs_messages, parse_mode='HTML')


# Function for processing messages
def handle_messages(update, context):
    if update.message:
        chat_id = update.message.chat_id
        user_id = update.message.from_user.id
        first_name = update.message.from_user.first_name
        username = update.message.from_user.username
        add_user(chat_id, user_id, first_name, username)
        update_last_message_date(chat_id, user_id)
    else:
        pass


# Main function for launching the bot
def main():
    # Create a connection to the database and create a table of users
    with sqlite3.connect('last_messages.db') as conn:
        c = conn.cursor()
        c.execute(
            "CREATE TABLE IF NOT EXISTS users (chat_id INTEGER, user_id INTEGER, first_name TEXT, username TEXT, last_message_date TEXT, PRIMARY KEY (chat_id, user_id))")
        conn.commit()

    # Create a bot and add handlers for commands and messages
    updater = Updater(TOKEN)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('last_messages', handle_last_messages_command))
    dp.add_handler(MessageHandler(Filters.all, handle_messages))

    # Launch the bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
